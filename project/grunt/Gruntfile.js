module.exports = function (grunt) {

  var currentdate = new Date();
  var datetime = "Last Sync: " + currentdate.getDate() + "/"
    + (currentdate.getMonth() + 1) + "/"
    + currentdate.getFullYear() + " @ "
    + currentdate.getHours() + ":"
    + currentdate.getMinutes() + ":"
    + currentdate.getSeconds();


  // Project configuration.
  grunt.initConfig({
    concat: {
      options: {
        separator: '\n',
        sourceMap: true,
        banner: "/*Processed by Grunt Files" + datetime + "*/\n"
      },
      css: {
        // src: ['../css/1.css', '../css/2.css', '../css/3.css'],
        src: ['../css/**/*.css'],
        dest: '../dist/style.css',
      },
      js: {
        src: ['./bower_components/jquery/dist/jquery.js', '../js/**/*.js'],
        dest: '../dist/app.js',
      },
      scss: {
        src: ['../scss/**/*.scss'],
        dest: '../dist/style.scss',
      }
    },


    cssmin: {
      options: {
        mergeIntoShorthands: false,
        roundingPrecision: -1
      },
      css: {
        files: {
          '../../htdocs/css/style.min.css': ['../dist/style.css']
        }
      },
      scss: {
        files: {
          '../../htdocs/css/app.css': ['../../htdocs/css/app.css']
        }
      }
    },


    sass: {
      dist: {
        options: {
          style: 'expanded'
        },
        files: {
          '../../htdocs/scss/app.css': ['../dist/style.scss']
        }
      }
    },


    uglify: {
      minify: {
        options: {
          sourceMap: true,
        },
        files: {
          '../../htdocs/js/app.min.js': ['../dist/app.js']
        }
      }
    },


    copy: {
      jquery: {
        files: [
          // includes files within path and its sub-directories
          { expand: true, flatten: true, filter: 'isFile', src: ['bower_components/jquery/dist/*'], dest: '../../htdocs/js/jquery/' },

        ],
      },
    },



    obfuscator: {
      options: {
        banner: '// obfuscated with grunt-contrib-obfuscator.\n',
        debugProtection: true,
        debugProtectionInterval: true,
        domainLock: ['kavin.selfmade.party']
      },
      task1: {
        options: {
          // options for each sub task
        },
        files: {
          '../../htdocs/js/app.min.o.js': [
            '../dist/app.js'
          ]
        }
      }
    },


    watch: {
      css: {
        files: ['../css/**/*.css'],
        tasks: ['concat:css', 'cssmin:css'],
        options: {
          spawn: false,
        },
      },
      js: {
        files: ['../js/**/*.js'],
        tasks: ['concat:js', 'uglify', 'obfuscator'],
        options: {
          spawn: false,
        },
      },
      scss: {
        files: ['../scss/**/*.scss'],
        tasks: ['concat:scss', 'cssmin:scss'],
        options: {
          spawn: false,
        },
      },
    },
  });

  // grunt.registerTask("HelloWorld", function () {
  //   console.log("I am grunt running...");
  // });

  // grunt.registerTask("task1", function () {
  //   console.log("I am task1 running...");
  // });

  // grunt.registerTask("task2", function () {
  //   console.log("I am task2 running...");
  // });

  grunt.loadNpmTasks('grunt-contrib-obfuscator');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-sass');

  // grunt.registerTask('css', ['concat:css', 'cssmin', 'sass']);
  // grunt.registerTask('js', ['concat:js', 'uglify', 'obfuscator']);

  grunt.registerTask('default', ['copy', 'concat', 'cssmin:css', 'sass', 'cssmin:scss',  'uglify', 'obfuscator', 'watch']);

}